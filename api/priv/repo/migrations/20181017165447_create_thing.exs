defmodule Api.Repo.Migrations.CreateThing do
  use Ecto.Migration

  def change do
    create table(:thing) do
      add :name, :string
      add :desc, :string
      add :size, :integer

      timestamps()
    end

  end
end
