defmodule Api.ContextTest do
  use Api.DataCase

  alias Api.Context

  describe "thing" do
    alias Api.Context.Things

    @valid_attrs %{desc: "some desc", name: "some name", size: 42}
    @update_attrs %{desc: "some updated desc", name: "some updated name", size: 43}
    @invalid_attrs %{desc: nil, name: nil, size: nil}

    def things_fixture(attrs \\ %{}) do
      {:ok, things} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Context.create_things()

      things
    end

    test "list_thing/0 returns all thing" do
      things = things_fixture()
      assert Context.list_thing() == [things]
    end

    test "get_things!/1 returns the things with given id" do
      things = things_fixture()
      assert Context.get_things!(things.id) == things
    end

    test "create_things/1 with valid data creates a things" do
      assert {:ok, %Things{} = things} = Context.create_things(@valid_attrs)
      assert things.desc == "some desc"
      assert things.name == "some name"
      assert things.size == 42
    end

    test "create_things/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Context.create_things(@invalid_attrs)
    end

    test "update_things/2 with valid data updates the things" do
      things = things_fixture()
      assert {:ok, things} = Context.update_things(things, @update_attrs)
      assert %Things{} = things
      assert things.desc == "some updated desc"
      assert things.name == "some updated name"
      assert things.size == 43
    end

    test "update_things/2 with invalid data returns error changeset" do
      things = things_fixture()
      assert {:error, %Ecto.Changeset{}} = Context.update_things(things, @invalid_attrs)
      assert things == Context.get_things!(things.id)
    end

    test "delete_things/1 deletes the things" do
      things = things_fixture()
      assert {:ok, %Things{}} = Context.delete_things(things)
      assert_raise Ecto.NoResultsError, fn -> Context.get_things!(things.id) end
    end

    test "change_things/1 returns a things changeset" do
      things = things_fixture()
      assert %Ecto.Changeset{} = Context.change_things(things)
    end
  end

  describe "diffthings" do
    alias Api.Context.DiffThing

    @valid_attrs %{}
    @update_attrs %{}
    @invalid_attrs %{}

    def diff_thing_fixture(attrs \\ %{}) do
      {:ok, diff_thing} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Context.create_diff_thing()

      diff_thing
    end

    test "list_diffthings/0 returns all diffthings" do
      diff_thing = diff_thing_fixture()
      assert Context.list_diffthings() == [diff_thing]
    end

    test "get_diff_thing!/1 returns the diff_thing with given id" do
      diff_thing = diff_thing_fixture()
      assert Context.get_diff_thing!(diff_thing.id) == diff_thing
    end

    test "create_diff_thing/1 with valid data creates a diff_thing" do
      assert {:ok, %DiffThing{} = diff_thing} = Context.create_diff_thing(@valid_attrs)
    end

    test "create_diff_thing/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Context.create_diff_thing(@invalid_attrs)
    end

    test "update_diff_thing/2 with valid data updates the diff_thing" do
      diff_thing = diff_thing_fixture()
      assert {:ok, diff_thing} = Context.update_diff_thing(diff_thing, @update_attrs)
      assert %DiffThing{} = diff_thing
    end

    test "update_diff_thing/2 with invalid data returns error changeset" do
      diff_thing = diff_thing_fixture()
      assert {:error, %Ecto.Changeset{}} = Context.update_diff_thing(diff_thing, @invalid_attrs)
      assert diff_thing == Context.get_diff_thing!(diff_thing.id)
    end

    test "delete_diff_thing/1 deletes the diff_thing" do
      diff_thing = diff_thing_fixture()
      assert {:ok, %DiffThing{}} = Context.delete_diff_thing(diff_thing)
      assert_raise Ecto.NoResultsError, fn -> Context.get_diff_thing!(diff_thing.id) end
    end

    test "change_diff_thing/1 returns a diff_thing changeset" do
      diff_thing = diff_thing_fixture()
      assert %Ecto.Changeset{} = Context.change_diff_thing(diff_thing)
    end
  end
end
