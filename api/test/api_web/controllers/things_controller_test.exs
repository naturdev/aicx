defmodule ApiWeb.ThingsControllerTest do
  use ApiWeb.ConnCase

  alias Api.Context
  alias Api.Context.Things

  @create_attrs %{desc: "some desc", name: "some name", size: 42}
  @update_attrs %{desc: "some updated desc", name: "some updated name", size: 43}
  @invalid_attrs %{desc: nil, name: nil, size: nil}

  def fixture(:things) do
    {:ok, things} = Context.create_things(@create_attrs)
    things
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all thing", %{conn: conn} do
      conn = get conn, things_path(conn, :index)
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create things" do
    test "renders things when data is valid", %{conn: conn} do
      conn = post conn, things_path(conn, :create), things: @create_attrs
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get conn, things_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "desc" => "some desc",
        "name" => "some name",
        "size" => 42}
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, things_path(conn, :create), things: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update things" do
    setup [:create_things]

    test "renders things when data is valid", %{conn: conn, things: %Things{id: id} = things} do
      conn = put conn, things_path(conn, :update, things), things: @update_attrs
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get conn, things_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "desc" => "some updated desc",
        "name" => "some updated name",
        "size" => 43}
    end

    test "renders errors when data is invalid", %{conn: conn, things: things} do
      conn = put conn, things_path(conn, :update, things), things: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete things" do
    setup [:create_things]

    test "deletes chosen things", %{conn: conn, things: things} do
      conn = delete conn, things_path(conn, :delete, things)
      assert response(conn, 204)
      assert_error_sent 404, fn ->
        get conn, things_path(conn, :show, things)
      end
    end
  end

  defp create_things(_) do
    things = fixture(:things)
    {:ok, things: things}
  end
end
