defmodule ApiWeb.DiffThingControllerTest do
  use ApiWeb.ConnCase

  alias Api.Context
  alias Api.Context.DiffThing

  @create_attrs %{}
  @update_attrs %{}
  @invalid_attrs %{}

  def fixture(:diff_thing) do
    {:ok, diff_thing} = Context.create_diff_thing(@create_attrs)
    diff_thing
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all diffthings", %{conn: conn} do
      conn = get conn, diff_thing_path(conn, :index)
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create diff_thing" do
    test "renders diff_thing when data is valid", %{conn: conn} do
      conn = post conn, diff_thing_path(conn, :create), diff_thing: @create_attrs
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get conn, diff_thing_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id}
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, diff_thing_path(conn, :create), diff_thing: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update diff_thing" do
    setup [:create_diff_thing]

    test "renders diff_thing when data is valid", %{conn: conn, diff_thing: %DiffThing{id: id} = diff_thing} do
      conn = put conn, diff_thing_path(conn, :update, diff_thing), diff_thing: @update_attrs
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get conn, diff_thing_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id}
    end

    test "renders errors when data is invalid", %{conn: conn, diff_thing: diff_thing} do
      conn = put conn, diff_thing_path(conn, :update, diff_thing), diff_thing: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete diff_thing" do
    setup [:create_diff_thing]

    test "deletes chosen diff_thing", %{conn: conn, diff_thing: diff_thing} do
      conn = delete conn, diff_thing_path(conn, :delete, diff_thing)
      assert response(conn, 204)
      assert_error_sent 404, fn ->
        get conn, diff_thing_path(conn, :show, diff_thing)
      end
    end
  end

  defp create_diff_thing(_) do
    diff_thing = fixture(:diff_thing)
    {:ok, diff_thing: diff_thing}
  end
end
