defmodule Api.Context.Things do
  use Ecto.Schema
  import Ecto.Changeset


  schema "thing" do
    field :desc, :string
    field :name, :string
    field :size, :integer

    timestamps()
  end

  @doc false
  def changeset(things, attrs) do
    things
    |> cast(attrs, [:name, :desc, :size])
    |> validate_required([:name, :desc, :size])
  end
end
