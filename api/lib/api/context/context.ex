defmodule Api.Context do
  @moduledoc """
  The Context context.
  """

  import Ecto.Query, warn: false
  alias Api.Repo

  alias Api.Context.Things

  @doc """
  Returns the list of thing.

  ## Examples

      iex> list_thing()
      [%Things{}, ...]

  """
  def list_thing do
    Repo.all(Things)
  end

  @doc """
  Gets a single things.

  Raises `Ecto.NoResultsError` if the Things does not exist.

  ## Examples

      iex> get_things!(123)
      %Things{}

      iex> get_things!(456)
      ** (Ecto.NoResultsError)

  """
  def get_things!(id), do: Repo.get!(Things, id)

  @doc """
  Creates a things.

  ## Examples

      iex> create_things(%{field: value})
      {:ok, %Things{}}

      iex> create_things(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_things(attrs \\ %{}) do
    %Things{}
    |> Things.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a things.

  ## Examples

      iex> update_things(things, %{field: new_value})
      {:ok, %Things{}}

      iex> update_things(things, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_things(%Things{} = things, attrs) do
    things
    |> Things.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Things.

  ## Examples

      iex> delete_things(things)
      {:ok, %Things{}}

      iex> delete_things(things)
      {:error, %Ecto.Changeset{}}

  """
  def delete_things(%Things{} = things) do
    Repo.delete(things)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking things changes.

  ## Examples

      iex> change_things(things)
      %Ecto.Changeset{source: %Things{}}

  """
  def change_things(%Things{} = things) do
    Things.changeset(things, %{})
  end

  alias Api.Context.DiffThing

  @doc """
  Returns the list of diffthings.

  ## Examples

      iex> list_diffthings()
      [%DiffThing{}, ...]

  """
  def list_diffthings do
    Repo.all(DiffThing)
  end

  @doc """
  Gets a single diff_thing.

  Raises `Ecto.NoResultsError` if the Diff thing does not exist.

  ## Examples

      iex> get_diff_thing!(123)
      %DiffThing{}

      iex> get_diff_thing!(456)
      ** (Ecto.NoResultsError)

  """
  def get_diff_thing!(id), do: Repo.get!(DiffThing, id)

  @doc """
  Creates a diff_thing.

  ## Examples

      iex> create_diff_thing(%{field: value})
      {:ok, %DiffThing{}}

      iex> create_diff_thing(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_diff_thing(attrs \\ %{}) do
    %DiffThing{}
    |> DiffThing.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a diff_thing.

  ## Examples

      iex> update_diff_thing(diff_thing, %{field: new_value})
      {:ok, %DiffThing{}}

      iex> update_diff_thing(diff_thing, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_diff_thing(%DiffThing{} = diff_thing, attrs) do
    diff_thing
    |> DiffThing.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a DiffThing.

  ## Examples

      iex> delete_diff_thing(diff_thing)
      {:ok, %DiffThing{}}

      iex> delete_diff_thing(diff_thing)
      {:error, %Ecto.Changeset{}}

  """
  def delete_diff_thing(%DiffThing{} = diff_thing) do
    Repo.delete(diff_thing)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking diff_thing changes.

  ## Examples

      iex> change_diff_thing(diff_thing)
      %Ecto.Changeset{source: %DiffThing{}}

  """
  def change_diff_thing(%DiffThing{} = diff_thing) do
    DiffThing.changeset(diff_thing, %{})
  end
end
