defmodule Api.Context.DiffThing do
  use Ecto.Schema
  import Ecto.Changeset


  schema "diffthings" do

    timestamps()
  end

  @doc false
  def changeset(diff_thing, attrs) do
    diff_thing
    |> cast(attrs, [])
    |> validate_required([])
  end
end
