defmodule ApiWeb.DiffThingView do
  use ApiWeb, :view
  alias ApiWeb.DiffThingView

  def render("index.json", %{diffthings: diffthings}) do
    %{data: render_many(diffthings, DiffThingView, "diff_thing.json")}
  end

  def render("show.json", %{diff_thing: diff_thing}) do
    %{data: render_one(diff_thing, DiffThingView, "diff_thing.json")}
  end

  def render("diff_thing.json", %{diff_thing: diff_thing}) do
    %{id: diff_thing.id}
  end
end
