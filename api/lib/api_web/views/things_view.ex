defmodule ApiWeb.ThingsView do
  use ApiWeb, :view
  alias ApiWeb.ThingsView

  def render("index.json", %{thing: thing}) do
    %{data: render_many(thing, ThingsView, "things.json")}
  end

  def render("show.json", %{things: things}) do
    %{data: render_one(things, ThingsView, "things.json")}
  end

  def render("things.json", %{things: things}) do
    %{id: things.id,
      name: things.name,
      desc: things.desc,
      size: things.size,
      time: things.updated_at}
  end
end
