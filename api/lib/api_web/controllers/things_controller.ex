defmodule ApiWeb.ThingsController do
  use ApiWeb, :controller

  alias Api.Context
  alias Api.Context.Things

  action_fallback ApiWeb.FallbackController

  def index(conn, _params) do
    thing = Context.list_thing()
    render(conn, "index.json", thing: thing)
  end

  def create(conn, %{"things" => things_params}) do
    with {:ok, %Things{} = things} <- Context.create_things(things_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", things_path(conn, :show, things))
      |> render("show.json", things: things)
    end
  end

  def show(conn, %{"id" => id}) do
    things = Context.get_things!(id)
    render(conn, "show.json", things: things)
  end

  def update(conn, %{"id" => id, "things" => things_params}) do
    things = Context.get_things!(id)

    with {:ok, %Things{} = things} <- Context.update_things(things, things_params) do
      render(conn, "show.json", things: things)
    end
  end

  def delete(conn, %{"id" => id}) do
    things = Context.get_things!(id)
    with {:ok, %Things{}} <- Context.delete_things(things) do
      send_resp(conn, :no_content, "")
    end
  end
end
