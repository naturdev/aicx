defmodule ApiWeb.DiffThingController do
  use ApiWeb, :controller

  alias Api.Context
  alias Api.Context.DiffThing

  action_fallback ApiWeb.FallbackController

  def index(conn, _params) do
    diffthings = Context.list_diffthings()
    render(conn, "index.json", diffthings: diffthings)
  end

  def create(conn, %{"diff_thing" => diff_thing_params}) do
    with {:ok, %DiffThing{} = diff_thing} <- Context.create_diff_thing(diff_thing_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", diff_thing_path(conn, :show, diff_thing))
      |> render("show.json", diff_thing: diff_thing)
    end
  end

  def show(conn, %{"id" => id}) do
    diff_thing = Context.get_diff_thing!(id)
    render(conn, "show.json", diff_thing: diff_thing)
  end

  def update(conn, %{"id" => id, "diff_thing" => diff_thing_params}) do
    diff_thing = Context.get_diff_thing!(id)

    with {:ok, %DiffThing{} = diff_thing} <- Context.update_diff_thing(diff_thing, diff_thing_params) do
      render(conn, "show.json", diff_thing: diff_thing)
    end
  end

  def delete(conn, %{"id" => id}) do
    diff_thing = Context.get_diff_thing!(id)
    with {:ok, %DiffThing{}} <- Context.delete_diff_thing(diff_thing) do
      send_resp(conn, :no_content, "")
    end
  end
end
