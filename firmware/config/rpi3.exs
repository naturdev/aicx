use Mix.Config

config :api,
  ecto_repos: [Api.Repo]

# Configure your database
config :api, Api.Repo,
  adapter: Sqlite.Ecto2,
  database: "/root/#{Mix.env}.sqlite3"
